# conda-build docker image

conda-build [docker] image to build [conda] packages.

Docker pull command:

```bash
docker pull registry.gitlab.com/maxiv/docker/conda-build
```

## How to use this image

To create a conda package, you must write a conda build recipe: <https://docs.conda.io/projects/conda-build/en/stable/user-guide/tutorials/build-pkgs.html>

[docker]: https://www.docker.com
[conda]: https://conda.io/docs/index.html
