FROM centos:7.9.2009

LABEL MAINTAINER "kits-sw@maxiv.lu.se"

ENV LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 LANGUAGE=en_US.UTF-8

# Use centos vault repo
RUN sed -i \
  -e "s/mirror.centos.org/vault.centos.org/g" \
  -e "s/^#.*baseurl=http/baseurl=http/g" \
  -e "s/^mirrorlist=http/#mirrorlist=http/g" /etc/yum.repos.d/CentOS-Base.repo

# Following libs required to import svgsynoptics2 (PyQt5.QtWebEngineWidgets):
# libXcomposite libXcursor libXrandr libXScrnSaver libXtst mesa-libGL pciutils
# mesa-libEGL and libglvnd-opengl needed by opencv
RUN yum update -y \
  && yum install -y epel-release \
  && yum install -y \
    which \
    bzip2 \
    tini \
    wget \
    make \
    patch  \
    tar \
    dpkg \
    mesa-libGL \
    mesa-libGLU \
    mesa-libEGL \
    libglvnd-opengl \
    libXcomposite \
    libXcursor \
    libXrandr \
    libXScrnSaver \
    libXtst \
    pciutils \
    xorg-x11-server-Xvfb \
  && yum clean all \
  && rm -rf /var/cache/yum \
  && mkdir /build

ENV MINIFORGE_INSTALLER_VERSION 24.7.1-0
ENV CONDA_VERSION 24.7.1
ENV CONDA_BUILD_VERSION 24.7.1
ENV MAMBA_VERSION 1.5.9
RUN curl -L -O "https://github.com/conda-forge/miniforge/releases/download/${MINIFORGE_INSTALLER_VERSION}/Miniforge3-${MINIFORGE_INSTALLER_VERSION}-Linux-x86_64.sh" \
  && /bin/bash "Miniforge3-${MINIFORGE_INSTALLER_VERSION}-Linux-x86_64.sh" -f -b -p /opt/conda \
  && rm "Miniforge3-${MINIFORGE_INSTALLER_VERSION}-Linux-x86_64.sh" \
  && /opt/conda/bin/mamba install -n base -y conda="${CONDA_VERSION}" mamba="${MAMBA_VERSION}" \
  # The git from CentOS is too old -> install it with conda
  && /opt/conda/bin/mamba install -n base -y conda-build="${CONDA_BUILD_VERSION}" git \
  && echo "conda ${CONDA_VERSION}" >> /opt/conda/conda-meta/pinned \
  && /opt/conda/bin/conda clean -ay \
  && ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh \
  && ln -s /opt/conda/etc/profile.d/mamba.sh /etc/profile.d/mamba.sh \
  && /opt/conda/bin/conda config --system --set auto_update_conda false \
  && /opt/conda/bin/conda config --system --set show_channel_urls true \
  && cat /opt/conda/.condarc \
  && /opt/conda/bin/conda info

ENV PATH /opt/conda/bin:$PATH

COPY entrypoint_source /entrypoint_source
COPY entrypoint /entrypoint

ENV GRAYSKULL_VERSION=2.7.1 \
    CONDA_RECIPE_MANAGER_VERSION=0.2.1

RUN mamba create -y -p /grayskull \
      python=3.12 \
      python-build \
      grayskull="${GRAYSKULL_VERSION}" \
      conda-recipe-manager="${CONDA_RECIPE_MANAGER_VERSION}" \
  && conda clean -ay

ENV RATTLER_BUID_VERSION=0.21.0
RUN mamba create -y -p /rattler \
      rattler-build="${RATTLER_BUID_VERSION}" \
  && conda clean -ay

# Env to run python -m setuptools_scm
RUN mamba create -y -p /setuptools \
      python=3.12 \
      setuptools \
      setuptools-scm \
  && conda clean -ay

WORKDIR /build

ENTRYPOINT [ "/usr/bin/tini", "--", "/entrypoint" ]
